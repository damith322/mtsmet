<?php
/**
 * Main Loop : Testimonials
 *
 * @package     Total
 * @subpackage  Partials
 * @author      Alexander Clarke
 * @copyright   Copyright (c) 2015, WPExplorer.com
 * @link        http://www.wpexplorer.com
 * @since       2.1.0
 * @version     2.1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Add to counter
global $wpex_count;
$wpex_count++;

	// Include template part
	get_template_part( 'partials/testimonials/testimonials-entry' );

// Clear Counter
if ( wpex_testimonials_archive_columns() == $wpex_count ) {
	$wpex_count=0;
}