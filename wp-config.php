<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sanmark_mtsmet');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&y+0pxUBkoFrPF!]=#W@|WEs}mP)&_nBo-uCh-&Kb9BB8tiLU3!Qj3(-jMKR;A9~');
define('SECURE_AUTH_KEY',  'Cejl31gO&H$Dc6l|#e&j<-#u1=&f7A-eOjtV,=p*-spfhF~A*n2xs>HMSq}/olp6');
define('LOGGED_IN_KEY',    'QqI6kO33-+:LnQyea`[-e5eqr1rD]DP*$gP|3A[,TXPH B.bWph~_+{G#L1^ss)+');
define('NONCE_KEY',        '`&`]<{uyX7;{y7WA1pMXtUgm~.],*_^i._ZE!djY][P=wmO$XpJFkk)XyT5?LP*8');
define('AUTH_SALT',        '&q*E l+h/H@|i7y?y!:Bs;9lZz_wTCY)t#FBn86It0:0aEj 2&4l_gJFcx6z!eZ?');
define('SECURE_AUTH_SALT', 'm{nR&mCZ-jZ5-UUGvyO1U>SsnHII|%~s,;!xg-Em0<]Q3k/f4R&K0p)+&x0+C_l8');
define('LOGGED_IN_SALT',   'AtFZSXFMxd0?w=mU4=NU0z~RHGz&$?I[5)?%!IkbUkSRhnIH^gV4,|t&99p#sjB/');
define('NONCE_SALT',       'PfpZ+DoIWaFr+7oqRh0.2YX;@a1n;koq.#]M|e>+p6DpPHc`tIdH;W{~p!f51 q!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
